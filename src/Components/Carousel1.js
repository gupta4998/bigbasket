import React from 'react';
import reactDom from 'react-dom';
import Carousel from 'react-bootstrap/Carousel';
import 'bootstrap/dist/css/bootstrap.min.css';
const Carousel1=()=>{
    return (

        <div>
            <Carousel >
                
  <Carousel.Item>
    <img
      className="d-block w-100 mx-5"
      src="https://www.bigbasket.com/media/uploads/banner_images/NPL525-27-29.jpg"
      alt="First slide"
    />
     </Carousel.Item>
  <Carousel.Item>
    <img
      className="d-block w-100 mx-5"
      src="https://www.bigbasket.com/media/uploads/banner_images/L1-NPL25-1200x300-25thmar.jpg"
      alt="Third slide"
    />  
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="d-block w-100 mx-5"
      src="https://www.bigbasket.com/media/uploads/banner_images/NPL338-41-44-47.jpg"
      alt="Third slide"
    />
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="d-block w-100 mx-5"
      src="https://www.bigbasket.com/media/uploads/banner_images/NPL310-12-19.jpg"
      alt="First slide"
    />
     </Carousel.Item>
     <Carousel.Item>
    <img
      className="d-block w-100 mx-5"
      src="https://www.bigbasket.com/media/uploads/banner_images/NPL533-35-37-39-41.jpg"
      alt="First slide"
    />
     </Carousel.Item>
     
</Carousel>

        </div>
    )
}
export default Carousel1;