import React from 'react';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Button from 'react-bootstrap/Button';
const Container1=()=>{
    return (
        <div class="col-4 ">
            
         <div class="underline green"> <p1>Category</p1></div>
         <hr></hr>
        <a class="link dim black green flex items-center  link dim  black   f6 f5-ns dib mr3 " href="#" title="Home">Bakery, Cakes and Dairy</a><br/>
        <a class="link dim black light-white ml-1 flex items-center  link dim  black  f6 f5-ns dib mr3 " href="#" title="Home">Bakery Snacks (6)</a><br/>
        <a class="link dim black light-white ml-1 flex items-center  link dim  black  f6 f5-ns dib mr3 " href="#" title="Home">Bread & Buns (37)</a><br/>
        <a class="link dim black light-white ml-1  flex items-center  link dim  black f6 f5-ns dib mr3 " href="#" title="Home">Cakes & Pastries (82)</a><br/>
        <a class="link dim black light-white ml-1  flex items-center  link dim  black f6 f5-ns dib mr3 " href="#" title="Home">Dairy (154)</a><br/>
        <a class="link dim black light-white ml-1 flex items-center  link dim  black  f6 f5-ns dib mr3 " href="#" title="Home">Cookies,Rusk & Khari (42)</a><br/>
                    <hr></hr>
                    <div class="underline green"> <p1>Brand</p1></div>
                    <Form inline>
                    <FormControl type="text" placeholder="Search by Brand" className="mr-sm-1   " />
                    <Button variant="outline-success">Search</Button>
                    </Form>
    <form class="pa1">
  
    <div class="flex items-center mb1 link dim  black">
      <input class="mr2" type="checkbox" id="spacejam" value="spacejam"/>
      <label for="spacejam" class="lh-copy">Akshayakalpa</label>
    </div>
    <div class="flex items-center mb2 link dim  black">
      <input class="mr2" type="checkbox" id="airbud" value="airbud"/>
      <label for="airbud" class="lh-copy">Amul</label>
    </div>
    <div class="flex items-center mb2 link dim  black">
      <input class="mr2" type="checkbox" id="hocuspocus" value="hocuspocus"/>
      <label for="hocuspocus" class="lh-copy">Amul Gold</label>
    </div>
    <div class="flex items-center mb2 link dim  black">
      <input class="mr2" type="checkbox" id="diehard" value="diehard"/>
      <label for="diehard" class="lh-copy">Amul Taaza</label>
    </div>
    <div class="flex items-center mb2 link dim  black">
      <input class="mr2" type="checkbox" id="primer" value="primer"/>
      <label for="primer" class="lh-copy">Basta</label>
    </div>
    <div class="flex items-center mb2 link dim  black">
      <input class="mr2" type="checkbox" id="proxy" value="proxy"/>
      <label for="proxy" class="lh-copy">Bon & Bread</label>
    </div>
    <div class="flex items-center mb2 link dim  black">
      <input class="mr2" type="checkbox" id="homealone" value="homealone"/>
      <label for="homealone" class="lh-copy">Elite</label>
    </div>
</form>
                    <hr></hr>
                    <div class="underline green"> <p1>Food Preference</p1></div>
                    <form class="pa1">
  
    <div class="flex items-center mb1 link dim  black">
      <input class="mr2" type="checkbox" id="spacejam" value="spacejam"/>
      <label for="spacejam" class="lh-copy">Vegeterian</label>
    </div>
    <div class="flex items-center mb2 link dim  black">
      <input class="mr2" type="checkbox" id="airbud" value="airbud"/>
      <label for="airbud" class="lh-copy">Non-Vegeterian</label>
    </div>
    <div class="flex items-center mb2 link dim  black">
      <input class="mr2" type="checkbox" id="hocuspocus" value="hocuspocus"/>
      <label for="hocuspocus" class="lh-copy">Contains Egg</label>
    </div>
    </form>
                    <hr></hr>
                    <div class="underline green"> <p1>Cuisine</p1></div>
                    <form class="pa1">
  
    <div class="flex items-center mb1 link dim  black">
      <input class="mr2" type="checkbox" id="spacejam" value="spacejam"/>
      <label for="spacejam" class="lh-copy">Italian</label>
    </div>
    <div class="flex items-center mb2 link dim  black">
      <input class="mr2" type="checkbox" id="airbud" value="airbud"/>
      <label for="airbud" class="lh-copy">American</label>
    </div>
    <div class="flex items-center mb2 link dim  black">
      <input class="mr2" type="checkbox" id="hocuspocus" value="hocuspocus"/>
      <label for="hocuspocus" class="lh-copy">French</label>
    </div>
    <div class="flex items-center mb2 link dim  black">
      <input class="mr2" type="checkbox" id="diehard" value="diehard"/>
      <label for="diehard" class="lh-copy">English</label>
    </div>
    <div class="flex items-center mb2 link dim  black">
      <input class="mr2" type="checkbox" id="primer" value="primer"/>
      <label for="primer" class="lh-copy">German</label>
    </div>
    <div class="flex items-center mb2 link dim  black">
      <input class="mr2" type="checkbox" id="proxy" value="proxy"/>
      <label for="proxy" class="lh-copy">South American</label>
    </div>
    <div class="flex items-center mb2 link dim  black">
      <input class="mr2" type="checkbox" id="homealone" value="homealone"/>
      <label for="homealone" class="lh-copy">Polish</label>
    </div>
</form>
                    <hr></hr>
                    <div class="underline green"> <p1>Price</p1></div>
                    <form class="pa1">
  
  <div class="flex items-center mb1 link dim  black">
    <input class="mr2" type="checkbox" id="spacejam" value="spacejam"/>
    <label for="spacejam" class="lh-copy">Less than Rs 20(55)</label>
  </div>
  <div class="flex items-center mb2 link dim  black">
    <input class="mr2" type="checkbox" id="airbud" value="airbud"/>
    <label for="airbud" class="lh-copy">Rs 21 to Rs 50(86)</label>
  </div>
  <div class="flex items-center mb2 link dim  black">
    <input class="mr2" type="checkbox" id="hocuspocus" value="hocuspocus"/>
    <label for="hocuspocus" class="lh-copy">Rs 51 to Rs 100(140)</label>
  </div>
  <div class="flex items-center mb2 link dim  black">
    <input class="mr2" type="checkbox" id="diehard" value="diehard"/>
    <label for="diehard" class="lh-copy">Rs 100 to Rs 150(111)</label>
  </div>
  <div class="flex items-center mb2 link dim  black">
    <input class="mr2" type="checkbox" id="primer" value="primer"/>
    <label for="primer" class="lh-copy">Rs 151 to Rs 200(189)</label>
  </div>
  </form>


                    </div>
                    
    )
}
export default Container1;