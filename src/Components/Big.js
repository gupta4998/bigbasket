import React from 'react';
import ReactDom from 'react-dom';
import 'tachyons';
import 'bootstrap/dist/css/bootstrap.min.css';
import Carousel1 from './Carousel1';
import Dropdown1 from './Dropdown1';
import Block from './Block';

import Navbar1 from './Navbar1';
import Offer from './Offer';
import Signin from './Signin';

const Big=()=>{
    return (
        <div>
            <Signin/>
            <Navbar1/> 
            <nav class="pa3 pa4-ns inline-flex">
            <hr></hr>
            <ul className="inline-flex">
           <Dropdown1/>
            <Offer/> 
            </ul>  
            <hr></hr>
            </nav>
             <hr ></hr>
            <Carousel1/>
            <hr></hr>
            <Block/>
            <hr></hr>
            <div className="mx-5">
               <div className=" ma2 bg-white dib ">
                <h1>Featured Product</h1>
               </div>
            </div>
            <hr></hr>
            {/* <Main/> */} 
       </div>   
    )
}
export default Big;