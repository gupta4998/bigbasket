import React,{Component} from 'react';
import { connect } from 'react-redux';
import { addToCart } from './Actions/cartAction';
class Home extends Component {
    handleClick = (id)=>{
        this.props.addToCart(id); 
    }
    render(){
        const itemList=this.props.items.map(item=>{
            return <div className=" ma1  dib btn-outline-primary pa1 tc " href="#0" key={item.id}>
            <img src={item.img} alt="Abhi" height="175px" width="175px"/>
            {/* <span to="/" className="btn-floating halfway-fab waves-effect waves-light red"><i className="material-icons">ADD Cart</i></span> */}
            <h4>{item.brand}</h4>
            <p>{item.product}</p>
            <p>{item.price}</p>
            <button onClick={()=>{this.handleClick(item.id)}}>Add</button>
    </div>
        })
        return (
            <div class="inline tc"> 
                {itemList}
            </div>
        )
    }
}
const mapStateToProps=(state)=>{
    return {
        items:state.items
    }
}
const mapDispatchToProps= (dispatch)=>{
    
    return{
        addToCart: (id)=>{dispatch(addToCart(id))}
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Home);