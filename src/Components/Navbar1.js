 import React,{Component} from 'react';
// import Reactdom from 'react-dom';
 import Navbar from 'react-bootstrap/Navbar';
 import Button from 'react-bootstrap/Button';
 import Form from 'react-bootstrap/Form';
 import FormControl from 'react-bootstrap/FormControl';
 import { Link } from 'react-router-dom';


  class Navbar1 extends Component{
    constructor(props){
      super(props);
    }
    render(){
    return (
     
     
        <div class="tc">
            <Navbar bg="light" expand="lg">
            <a class="db dtc v mid-gray link dim w-10 w-25-l mx-4 mb2 mb0-l pr-3" href="#" title="Home">

    <Link to ="/"><img src="https://image4.owler.com/logo/bigbasket_owler_20190729_092819_original.jpg" class="dib " alt="Site Name" height="60px" weidth="60px"/></Link>
  </a>
  
  <Navbar.Toggle aria-controls="basic-navbar-nav" />
  <Navbar.Collapse id="basic-navbar-nav">
    
    <Form inline>
      <FormControl type="text" placeholder="Search" className="mr-sm-3   " />
      <Button variant="outline-success">Search</Button>
    </Form>
    <a class="link dim dark-red f3 f5-l dib mr5 mr4-l mx-2" href="#"><Link to="/"><img src="https://www.bigbasket.com/media/uploads/banner_images/All_bbstar_DT_1_150x30_23rdOct.png" class="w-5" alt="night sky over land" height="40px" weidth="30px"/></Link></a>
    <div className="mx-5">
        <ul className="right">
    <button className="link btn-outline-light f8 dib mr4 pa3 mr4-1 hover-bg-light "><Link to="/">Shop</Link></button>
    <button className="link btn-outline-light f8 dib mr4 pa3 mr4-1 hover-bg-light"><Link to="/cart">My cart</Link></button>
    </ul>
    </div>
     </Navbar.Collapse>
</Navbar>

        </div>
      
       
    )
    }
}
export default Navbar1;

 
