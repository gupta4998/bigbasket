import { ADD_TO_CART,REMOVE_ITEM,SUB_QUANTITY,ADD_QUANTITY,ADD_SHIPPING,SUB_SHIPPING } from '../Actions/cart-action'
const iState ={
    items:[
        {
            id:1,
            img:'https://www.bigbasket.com/media/uploads/p/s/242671_1-nandini-goodlife-toned-milk.jpg',
            brand:'Milky Mist',
            product:'Curd  500 g',
            price:40
        },
        {
            id:2,
            img:'https://www.bigbasket.com/media/uploads/p/s/104860_6-amul-butter-pasteurised.jpg',
            brand:'Fresho Signature',
            product:'Whole Wheat Bread ',
            price:35
        },
        {
            id:3,
            img:'https://www.bigbasket.com/media/uploads/p/s/100285703_15-nandini-goodlife-toned-milk.jpg',
            brand:'Fresho Signature',
            product:'Corn Bread 300 g',
            price:79
        },
        {
            id:4,
            img:'https://www.bigbasket.com/media/uploads/p/s/148715_1-amul-taaza-toned-milk.jpg',
            brand:'Nandini GoodLife',
            product:'Skimmed Milk',
            price:26
        },
        {
            id:5,
            img:'https://www.bigbasket.com/media/uploads/p/s/40131633_2-akshayakalpa-malai-paneer-organic.jpg',
            brand:'Amul Taaza',
            product:'Fresh Toned Milk',
            price:22
        },
    
        {
            id:6,
            img:'https://www.bigbasket.com/media/uploads/p/s/40087525_2-fresho-sandwich-bread-safe-preservative-free.jpg',
            brand:'Fresho',
            product:'Sandwich Bread',
            price:29
        },
        {
            id:7,
            img:'https://www.bigbasket.com/media/uploads/p/s/100011680_8-nestle-a-nourish-toned-milk.jpg',
            brand:'Nestle A+',
            product:'Nourish Toned Milk',
            price:83
        },
        {
            id:8,
            img:'https://www.bigbasket.com/media/uploads/p/mm/40025357_3-fresho-pav-safe-preservative-free.jpg',
            brand:'Fesho',
            product:'Pav - Safe, Preservative ',
            price:29
        },
        {
            id:9,
            img:'https://www.bigbasket.com/media/uploads/p/mm/40037482_3-fresho-signature-fruit-tea-cake.jpg',
            brand:'Fesho-Signature',
            product:'Fruit Tea Cake',
            price:149
        },
        {
            id:10,
            img:'https://www.bigbasket.com/media/uploads/p/mm/40088412_2-hersheys-milk-shake-chocolate.jpg',
            brand:'Hersheys',
            product:'Milk Shake - Chocolate',
            price:99
        },
        {
            id:11,
            img:'https://www.bigbasket.com/media/uploads/p/mm/40037495_2-fresho-signature-muffincup-cake-choco-chip.jpg',
            brand:'Fresho Signature',
            product:'Muffin/Cup Cake - Choco Chip',
            price:139
        },
        {
            id:12,
            img:'https://www.bigbasket.com/media/uploads/p/mm/259977_15-britannia-fruit-cake.jpg',
            brand:'Britannia',
            product:'Fruit Cake',
            price:40
        },
        {
            id:13,
            img:'https://www.bigbasket.com/media/uploads/p/mm/40006627_2-milky-mist-cooking-butter-unsalted.jpg',
            brand:'Milky Mist',
            product:'Cooking Butter - Unsalted',
            price:280
        },
        {
            id:14,
            img:'https://www.bigbasket.com/media/uploads/p/mm/1207141_1-nestle-milo-cocoa-malt-milk-beverage-grab-go-pack.jpg',
            brand:'Nestle',
            product:'Milo Cocoa',
            price:210
        }, {
            id:15,
            img:'https://www.bigbasket.com/media/uploads/p/mm/1207142_1-nescafe-chilled-latte-coffee.jpg',
            brand:'Nescafe',
            product:'Chilled Latte Coffee',
            price:220
        },
        {
            id:16,
            img:'https://www.bigbasket.com/media/uploads/p/mm/40131634_1-akshayakalpa-cheese-organic-cheddar-mild-plain-young.jpg',
            brand:'Akshayakalpa',
            product:'Cheese',
            price:189
        },
        {
            id:17,
            img:'https://www.bigbasket.com/media/uploads/p/mm/30007659_5-elite-rusk-milk.jpg',
            brand:'Elite',
            product:'Rusk - Milk',
            price:20
        },
        {
            id:18,
            img:'https://www.bigbasket.com/media/uploads/p/mm/40121096_2-milky-mist-cream-fresh.jpg',
            brand:'Milky-Mist',
            product:'Cream',
            price:99
        },
        {
            id:19,
            img:'https://www.bigbasket.com/media/uploads/p/mm/40119176_1-bauli-moonfils-strawberry.jpg',
            brand:'Bauli',
            product:'Moonfils - Strawberry',
            price:37
        },
        {
            id:20,
            img:'https://www.bigbasket.com/media/uploads/p/mm/40037482_3-fresho-signature-fruit-tea-cake.jpg',
            brand:'Fesho-Signature',
            product:'Fruit Tea Cake',
            price:49
        },
        {
            id:21,
            img:'https://www.bigbasket.com/media/uploads/p/s/40003162_2-milky-mist-curd-farm-fresh.jpg',
            brand:'Milky Mist',
            product:'Curd  500 g',
            price:40
        },
        {
            id:22,
            img:'https://www.bigbasket.com/media/uploads/p/s/40025355_6-fresho-whole-wheat-bread-safe-preservative-free.jpg',
            brand:'Fresho Signature',
            product:'Whole Wheat Bread ',
            price:35
        },
        {
            id:23,
            img:'https://www.bigbasket.com/media/uploads/p/s/40037478_2-fresho-signature-corn-bread-pre-sliced.jpg',
            brand:'Fresho Signature',
            product:'Corn Bread 300 g',
            price:79
        },
        {
            id:24,
            img:'https://www.bigbasket.com/media/uploads/p/s/242673_1-nandini-goodlife-skimmed-milk.jpg',
            brand:'Nandini GoodLife',
            product:'Skimmed Milk',
            price:26
        },
        {
            id:25,
            img:'https://www.bigbasket.com/media/uploads/p/s/70001832_2-amul-taaza-fresh-toned-milk.jpg',
            brand:'Amul Taaza',
            product:'Fresh Toned Milk',
            price:22
        }
    ],
    addedItems:[],
    total: 0

}
const cartReducer= (state = iState,action)=>{
   
    //INSIDE HOME COMPONENT
    if(action.type === ADD_TO_CART){
          let addedItem = state.items.find(item=> item.id === action.id)
          //check if the action id exists in the addedItems
         let existed_item= state.addedItems.find(item=> action.id === item.id)
         if(existed_item)
         {
            addedItem.quantity += 1 
             return{
                ...state,
                 total: state.total + addedItem.price 
                  }
        }
         else{
            addedItem.quantity = 1;
            //calculating the total
            let newTotal = state.total + addedItem.price 
            
            return{
                ...state,
                addedItems: [...state.addedItems, addedItem],
                total : newTotal
            }
            
        }
    }
    if(action.type === REMOVE_ITEM){
        let itemToRemove= state.addedItems.find(item=> action.id === item.id)
        let new_items = state.addedItems.filter(item=> action.id !== item.id)
        
        //calculating the total
        let newTotal = state.total - (itemToRemove.price * itemToRemove.quantity )
        console.log(itemToRemove)
        return{
            ...state,
            addedItems: new_items,
            total: newTotal
        }
    }
    //INSIDE CART COMPONENT
    if(action.type=== ADD_QUANTITY){
        let addedItem = state.items.find(item=> item.id === action.id)
          addedItem.quantity += 1 
          let newTotal = state.total + addedItem.price
          return{
              ...state,
              total: newTotal
          }
    }
    if(action.type=== SUB_QUANTITY){  
        let addedItem = state.items.find(item=> item.id === action.id) 
        //if the qt == 0 then it should be removed
        if(addedItem.quantity === 1){
            let new_items = state.addedItems.filter(item=>item.id !== action.id)
            let newTotal = state.total - addedItem.price
            return{
                ...state,
                addedItems: new_items,
                total: newTotal
            }
        }
        else {
            addedItem.quantity -= 1
            let newTotal = state.total - addedItem.price
            return{
                ...state,
                total: newTotal
            }
        }
        
    }

    if(action.type=== ADD_SHIPPING){
          return{
              ...state,
              total: state.total + 6
          }
    }

    if(action.type=== SUB_SHIPPING){
        return{
            ...state,
            total: state.total - 6
        }
  }
    
  else{
    return state
    }
    
}

export default cartReducer
