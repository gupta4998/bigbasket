import React from 'react';
import logo from './logo.svg';
import './App.css';
import Container from 'react-bootstrap/Container';
import {BrowserRouter,Route,Switch} from 'react-router-dom';
import Big from './Components/Big';
import Sidebar from './Components/Sidebar';
import Home from './Components/Home';
import Cart from './Components/Cart';


function App() {
  return (
    <BrowserRouter>
    <div className="App">
      <Big/>
      <div>
      <Container>
                <div class="row">
                    <Sidebar/>
                        <div class="col-8 ">
                        <Switch>
                     <Route exact path="/" component={Home}/>
                    <Route path="/cart" component={Cart}/> 
                   
                  </Switch>
                        </div>
                    
                 </div>
      </Container>
            <hr></hr>
      </div>


      
    </div>
    </BrowserRouter>
  );
}

export default App;
